import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from '@/router'
import Axios from 'axios'



import 'vuetify/dist/vuetify.min.css'
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/General.css'
import '@/styles/Vuetify.css'

import Vuetify from 'vuetify'
Vue.use(Vuetify)

import Element from 'element-ui'
Vue.use(Element)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
