import Vue from 'vue'
import VueRouter from 'vue-router'

// Pages
import Home from '@/pages/Home'
import Fail from '@/pages/Fail'
import CNAE from '@/pages/CNAE'
import Zones from '@/pages/Zones'

Vue.use(VueRouter)
const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      name: 'home',
      path: '/',
      component: Home,
      meta: {
        title: 'Empreenda Aqui',
      },
    },

    {
      name: 'fail',
      path: '/error',
      component: Fail,
      meta: {
        title: 'Ops! Área não encontrada',
      },
      props: true
    },

    {
      name: 'cnae',
      path: '/cnae',
      component: CNAE,
      meta: {
        title: 'Defina o CNAEs',
      },
      props: true
    },

    {
      name: 'zones',
      path: '/zones',
      component: Zones,
      meta: {
        title: 'Veja mais sobre seu endereço',
      },
      props: true
    },

    // { path: '*', redirect: '/login' }
  ]
})



export default router